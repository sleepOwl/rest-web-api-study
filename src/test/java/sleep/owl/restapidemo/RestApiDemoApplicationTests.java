package sleep.owl.restapidemo;

import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.restdocs.mockmvc.MockMvcRestDocumentation;
import org.springframework.restdocs.mockmvc.RestDocumentationResultHandler;
import org.springframework.security.oauth2.common.util.JacksonJsonParser;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.context.WebApplicationContext;
import sleep.owl.RestApiDemoApplication;

import javax.servlet.FilterChain;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessRequest;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessResponse;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebAppConfiguration
@SpringBootTest(classes = RestApiDemoApplication.class)
@Slf4j
public class RestApiDemoApplicationTests {

    //clientId
    final static String CLIENT_ID = "client_2";
    //clientSecret
    final static String CLIENT_SECRET = "123456";
    //用户名
    final static String USERNAME = "user_1";
    //密码
    final static String PASSWORD = "123456";

    private static final String CONTENT_TYPE = "application/json;charset=UTF-8";
    @Rule
    public JUnitRestDocumentation restDocumentation = new JUnitRestDocumentation();
    private MockMvc mockMvc;
    private RestDocumentationResultHandler dc;

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private FilterChainProxy springSecurityFilterChain;

    @Before
    public void setUp() {
        dc = document("{method-name}",
                preprocessRequest(prettyPrint())
                , preprocessResponse(prettyPrint()));


        // 配置mockmvc
        this.mockMvc = MockMvcBuilders
                .webAppContextSetup(this.context)
                .addFilter(springSecurityFilterChain)
                .apply(
                        documentationConfiguration(this.restDocumentation)
                                .operationPreprocessors()
                                .withRequestDefaults(prettyPrint())
                                .withResponseDefaults(prettyPrint())
                ).alwaysDo(print())
                .alwaysDo(this.dc)
                .build();
    }

    @Test
    public void contextLoads() throws Exception {
        this.mockMvc.perform(get("/notFoundTest"))
                .andDo(document("notFoundTest").document(

                ));
    }

    @Test
    public void saveUser() throws Exception {
        this.mockMvc.perform(
                post("/users")
                        .param("name", "王二")
                        .param("age", "10")
                        .param("code", "1000")
        ).andDo(
                document("saveUser")
        );

    }


    @Test
    public void findUser() throws Exception {
     /*   this.mockMvc.perform(
                post("/users")
                        .param("access_token", obtainAccessToken())
                        .param("name", "王二")
                        .param("age", "10")
                        .param("code", "1000")
        );*/
        this.mockMvc.perform(
                get("/users")
//                        .param("name", "王二")
                        .param("access_token", obtainAccessToken())
        ).andDo(
                dc.document(
                        responseFields(
                                fieldWithPath("[].id").description("id").optional(),
                                fieldWithPath("[].createTime").description("创建时间").optional(),
                                fieldWithPath("[].updateTime").description("更新时间").optional(),
                                fieldWithPath("[].name").description("姓名").optional(),
                                fieldWithPath("[].age").description("年龄"),
                                fieldWithPath("[].code").description("代码"),
                                fieldWithPath("[].password").description("密码"),
                                fieldWithPath("[].loginName").description("登陆名称"),
                                fieldWithPath("[].roles").description("角色信息"),
                                fieldWithPath("[].username").description("用户名"),
                                fieldWithPath("[].accountNonExpired").description("是否异常"),
                                fieldWithPath("[].accountNonLocked").description("是否锁定"),
                                fieldWithPath("[].credentialsNonExpired").description("关系"),
                                fieldWithPath("[].authorities").description("作者"),
                                fieldWithPath("[].enabled").description("可用")
                        )
                )

        );


    }

    @Test
    public void getAccessToken() throws Exception {
        final String accessToken = obtainAccessToken();
        log.info("=============={}==============", accessToken);

    }

    /**
     * 模拟tocken的获取
     *
     * @return
     * @throws Exception
     */
    public String obtainAccessToken() throws Exception {
        final MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("grant_type", "password");
        params.add("client_id", CLIENT_ID);
        params.add("username", USERNAME);
        params.add("password", PASSWORD);
        params.add("scope", "select");
        params.add("client_secret", "123456");
        // @formatter:off
        ResultActions result = mockMvc.perform(post("/oauth/token")
                .params(params)
                .with(httpBasic(CLIENT_ID, CLIENT_SECRET))
                .accept(CONTENT_TYPE))
                .andExpect(status().isOk())
                .andExpect(content().contentType(CONTENT_TYPE));

        // @formatter:on
        String resultString = result.andReturn().getResponse().getContentAsString();
        JacksonJsonParser jsonParser = new JacksonJsonParser();
//        System.out.println(jsonParser.parseMap(resultString).get("access_token").toString());
        return jsonParser.parseMap(resultString).get("access_token").toString();
    }
}

