INSERT INTO `SYS_USER` ( `id`, `create_time`, `update_time`, `AGE`, `CODE`, `LOGIN_NAME`, 	`NAME`, `PASSWORD`)
VALUES
	( '1', '2018-12-22 11:44:00', '2018-12-22 11:44:00', '10', '1000001', 'admin', '史恭文','123456' );

INSERT INTO `role` ( `id`, `create_time`, `update_time`, `name`)
VALUES
	( '1', '2018-12-22 11:44:00', '2018-12-22 11:44:00', 'admin');


INSERT INTO `sys_permission` ( `id`, `create_time`, `update_time`,`roleId`,`name`,`descritpion`,`url`,`pid`)
VALUES
	( '1', '2018-12-22 11:44:00', '2018-12-22 11:44:00', 1,'管理员','管理员权限','https://www.baidu.com',null);

-- oauth 服务配置

INSERT INTO `oauth_client_details`
(`client_id`, `create_time`, `update_time`, `access_token_validity`, `additional_information`, `authorities`, `authorized_grant_types`, `autoapprove`, `client_secret`, `refresh_token_validity`, `resource_ids`, `scope`, `web_server_redirect_uri`)
VALUES
('client_1', NULL, NULL, NULL, NULL, 'client', 'client_credentials', NULL, '123456', 3600, 'order',  '.*'   , NULL);


INSERT INTO `oauth_client_details`
(`client_id`, `create_time`, `update_time`, `access_token_validity`, `additional_information`, `authorities`, `authorized_grant_types`, `autoapprove`, `client_secret`, `refresh_token_validity`, `resource_ids`, `scope`, `web_server_redirect_uri`)
VALUES
('client_2', NULL, NULL, NULL, NULL, 'client', 'password',           NULL, '123456', 3600, 'order', 'select', NULL);


INSERT INTO `oauth_client_details`
(`client_id`, `create_time`, `update_time`, `access_token_validity`, `additional_information`, `authorities`, `authorized_grant_types`, `autoapprove`, `client_secret`, `refresh_token_validity`, `resource_ids`, `scope`, `web_server_redirect_uri`)
VALUES
('client_3', NULL, NULL, NULL, NULL, 'client', 'password',           NULL, '123456', 3600, 'order', 'select', NULL);
