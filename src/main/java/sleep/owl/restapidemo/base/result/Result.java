package sleep.owl.restapidemo.base.result;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.http.HttpStatus;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@ToString
public class Result implements Serializable {
    @Getter
    @Setter
    private Object data;
    @Getter
    @Setter
    private HttpStatus status;
    @Getter
    @Setter
    private Object msg;

    @Getter
    @Setter
    private Integer code;

    private Map<String, Object> resultMap;
    private List<Object> resultList;

    public Result addObj(String key, Object value) {
        if (resultMap == null) {
            resultMap = new HashMap<>();
            this.data = resultMap;
        }
        resultMap.put(key, value);
        return this;
    }

    public Result addObj(Object object) {
        if (resultList == null) {
            resultList = new ArrayList<>();
            this.data = resultList;
        }
        resultList.add(object);
        return this;
    }

    public static Result error(Object msg) {
        Result result = new Result();
        result.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
        result.setMsg(msg);
        return result;
    }

    public static Result success(Object data) {
        Result result = new Result();
        result.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
        result.setData(data);
        return result;
    }

    public static Result success() {
        Result result = new Result();
        result.setStatus(HttpStatus.OK);
        return result;
    }

    public Result setData(Object data) {
        this.data = data;
        return this;
    }

    public Result setMsg(Object msg) {
        this.msg = msg;
        return this;
    }


}
