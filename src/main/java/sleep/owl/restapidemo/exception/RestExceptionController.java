package sleep.owl.restapidemo.exception;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import sleep.owl.restapidemo.base.result.Result;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@ControllerAdvice
public class RestExceptionController {
    @Autowired
    private ErrorAttributes errorAttributes;

    /**
     * 处理异常
     */
    @ExceptionHandler(value = Exception.class)
    public ResponseEntity<Result> baseErrorHandler(HttpServletRequest req, Exception e) {
        Result result = new Result();
        if (e instanceof NoHandlerFoundException) {
            result.setStatus(HttpStatus.NOT_FOUND);
            result.setMsg("地址未找到");
        } else {
            HttpStatus status = getStatus(req);
            result.setStatus(status);
            result.setMsg("服务器内部异常,请联系管理员");
        }
        result.setCode(result.getStatus().value());
        return new ResponseEntity<>(result, result.getStatus());
    }

    private Map<String, Object> getErrorAttributes(HttpServletRequest request, boolean includeStackTrace) {
        WebRequest webRequest = new ServletWebRequest(request);
        return this.errorAttributes.getErrorAttributes(webRequest, includeStackTrace);
    }

    protected HttpStatus getStatus(HttpServletRequest request) {
        Integer statusCode = (Integer) request
                .getAttribute("javax.servlet.error.status_code");
        if (statusCode == null) {
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }
        try {
            return HttpStatus.valueOf(statusCode);
        } catch (Exception ex) {
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }
    }

}
