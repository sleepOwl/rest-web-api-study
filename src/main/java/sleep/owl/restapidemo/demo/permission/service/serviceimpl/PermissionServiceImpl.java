package sleep.owl.restapidemo.demo.permission.service.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sleep.owl.restapidemo.demo.permission.dao.PermissionRepository;
import sleep.owl.restapidemo.demo.permission.model.Permission;
import sleep.owl.restapidemo.demo.permission.service.PermissionService;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;

@Service
public class PermissionServiceImpl implements PermissionService {
    @Autowired
    private PermissionRepository permissionRepository;

    @Override
    public List<Permission> findAll() {
        return permissionRepository.findAll();
    }

    @Override
    public List<Permission> findByAdminUserId(String userId) {
        return null;
    }
}
