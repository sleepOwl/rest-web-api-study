package sleep.owl.restapidemo.demo.permission.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sleep.owl.restapidemo.demo.permission.model.Permission;

@Repository
public interface PermissionRepository extends JpaRepository<Permission, String> {
}
