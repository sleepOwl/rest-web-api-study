package sleep.owl.restapidemo.demo.permission.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import sleep.owl.restapidemo.base.BaseModel;

import javax.persistence.*;

@Entity
@Data
@ToString
@Table(name = "sys_permission")
@EqualsAndHashCode
public class Permission extends BaseModel {
    private String roleId;
    //权限名称
    private String name;

    //权限描述
    private String descritpion;

    //授权链接
    private String url;

    //父节点id
    private int pid;
}
