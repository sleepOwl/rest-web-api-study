package sleep.owl.restapidemo.demo.permission.service;

import sleep.owl.restapidemo.demo.permission.model.Permission;

import java.util.List;

public interface PermissionService {
    List<Permission> findAll();

    List<Permission> findByAdminUserId(String userId);
}
