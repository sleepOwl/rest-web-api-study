package sleep.owl.restapidemo.demo.role;

import lombok.Data;
import lombok.ToString;
import org.springframework.security.core.GrantedAuthority;
import sleep.owl.restapidemo.base.BaseModel;

import javax.persistence.Entity;

@Entity
@Data
@ToString
public class Role extends BaseModel implements GrantedAuthority {
    private String name;

    @Override
    public String getAuthority() {
        return name;
    }
}
