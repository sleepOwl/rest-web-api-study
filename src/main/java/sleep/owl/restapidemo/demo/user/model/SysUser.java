package sleep.owl.restapidemo.demo.user.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import sleep.owl.restapidemo.base.BaseModel;
import sleep.owl.restapidemo.demo.role.Role;

import javax.persistence.*;
import java.util.Collection;
import java.util.List;

@Data
@ToString
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class SysUser extends BaseModel implements UserDetails {
    private String name;
    private Integer age;
    private String code;
    private String password;
    private String loginName;
    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "user_role", joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"))
    private List<Role> roles;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return roles;
    }

    @Override
    public String getUsername() {
        return name;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
