package sleep.owl.restapidemo.demo.user.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import sleep.owl.restapidemo.demo.user.model.SysUser;
import sleep.owl.restapidemo.demo.user.dao.UserRepository;

import java.util.List;
import java.util.Objects;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    public List<SysUser> getUsers(SysUser user) {
        if (Objects.nonNull(user)) {
            Example<SysUser> example = Example.of(user);
            return userRepository.findAll(example);
        } else {
            return userRepository.findAll();
        }
    }

    public void save(SysUser user) {
        userRepository.save(user);
    }

    public void delete(String id) {
        userRepository.deleteById(id);
    }


    public void update(SysUser user) {
        userRepository.save(user);
    }

    public void deletes(SysUser user) {
        if (Objects.nonNull(user)) {
            Example<SysUser> example = Example.of(user);
            List<SysUser> users = userRepository.findAll(example);
            if (!users.isEmpty()) {
                for (SysUser delUser : users) {
                    userRepository.delete(delUser);
                }
            }
        }
    }

    public SysUser getUser(String id) {
        return userRepository.findById(id).get();
    }

    public SysUser getUserByName(String name) {
        return userRepository.findByLoginName(name);
    }
}
