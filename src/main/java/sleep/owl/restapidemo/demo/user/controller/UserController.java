package sleep.owl.restapidemo.demo.user.controller;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import sleep.owl.restapidemo.demo.user.model.SysUser;
import sleep.owl.restapidemo.demo.user.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Objects;

@Log4j2
@RestController
@RequestMapping("users")
public class UserController {
    @Autowired
    private UserService userService;
    @Autowired
    private HttpServletRequest request;

    /**
     * get 方法用于信息的检索
     * 获取所有的用户
     */
    @GetMapping
    public List<SysUser> getUsers(SysUser user) {
        return userService.getUsers(user);
    }

    /**
     * 单个用户单个资源的获取形式
     *
     * @param id
     * @return
     */
    @GetMapping("{id}")
    public SysUser getUser(@PathVariable("id") String id) {
        return userService.getUser(id);
    }

    /**
     * put方法用户信息的创建
     * 保存用户
     *
     * @param user
     */
    @PutMapping
    public void save(SysUser user) {
        userService.save(user);
    }

    /**
     * 使用路径请参数的方式来指明删除的是某个资源
     *
     * @param id
     */
    @DeleteMapping("{id}")
    public void delete(@PathVariable("id") String id) {
        userService.delete(id);
    }

    /**
     * 无请求路径的批量删除,按照查询条件进行删除
     *
     * @param user
     */
    @DeleteMapping
    public void deleteAll(SysUser user) {
        userService.deletes(user);
    }

    /**
     * 局部字段的更新
     *
     * @param user
     */
    @PatchMapping
    public void update(SysUser user) {
        if (Objects.isNull(user) || StringUtils.isEmpty(user.getId())) {
            throw new NullPointerException("用户id 和用户不可为空");
        }
        userService.update(user);
    }

}
