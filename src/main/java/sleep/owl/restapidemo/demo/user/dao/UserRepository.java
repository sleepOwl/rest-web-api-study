package sleep.owl.restapidemo.demo.user.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sleep.owl.restapidemo.demo.user.model.SysUser;

@Repository
public interface UserRepository extends JpaRepository<SysUser, String> {
    SysUser findByLoginName(String name);
}
