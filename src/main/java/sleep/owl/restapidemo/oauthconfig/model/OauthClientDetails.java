package sleep.owl.restapidemo.oauthconfig.model;

import lombok.Data;
import lombok.ToString;
import sleep.owl.restapidemo.base.BaseModel;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
//资源服务器ID
@AttributeOverride(name = "id", column = @Column(name = "CLIENT_ID", length = 48))
@Data
@ToString
public class OauthClientDetails extends BaseModel {
    // 资源ID
    @Column(length = 256, name = "resource_ids")
    private String resourceIds;
    // 客户端密码
    @Column(length = 256, name = "client_secret")
    private String clientSecret;
    //访问范围（权限）
    @Column(length = 256, name = "scope")
    private String scope;
    // 验证类型 密码模式，客户端模式，令牌模式。。。
    @Column(length = 256, name = "authorized_grant_types")
    private String authorizedGrantTypes;
    // 重定向地址，用作单点登陆
    @Column(length = 256, name = "web_server_redirect_uri")
    private String webServerRedirectUri;
    // 令牌过期时间
    @Column(length = 256, name = "authorities")
    private String authorities;
    @Column(length = 11, name = "access_token_validity")
    private Integer accessTokenValidity;
    // 允许令牌刷新
    @Column(length = 11, name = "refresh_token_validity")
    private Integer refreshTokenValidity;
    @Column(length = 256, name = "additional_information")
    private String additionalInformation;
    @Column(length = 256, name = "autoapprove")
    private String autoapprove;
}
