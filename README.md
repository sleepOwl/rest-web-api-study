# rest-web-api-study
## 项目说明:
学习restfulapi 风格的接口开发demo实例
1. 对异常的统一处理,
2. 集成ouath2 进行身份的验证
3. 使用spring rest doc 进行文档的生成

## 异常处理的实现:

参照源码中的: RestExceptionController.class   
spring 中有对restapi 的支持,`@ControllerAdvice`注解可以使一个类具有异常处理的功能,在该类的方法中使用`  @ExceptionHandler(value = Exception.class)`注解该方法将根据value中配置的异常类型拦截请求以实现统一的异常处理

## 集成ouath2

ouath2是一个协议,spring-security中包含了该协议的实现,通过简单的配置就可以实现一个ouath协议的服务器,我再本项目中配置了两种身份验证的形式,一种是密码模式的另一种使用的是客户端模式,密码模式主要应用于登陆,客户端模式主要应用于api接口的调用

## 集成 spring rest doc  

rest doc 主要思想是在测试的时候api就可生成了,doc可以在多种测试框架中实现了集成,当前项目中使用的是mockmvc的支持