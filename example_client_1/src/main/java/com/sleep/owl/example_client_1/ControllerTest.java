package com.sleep.owl.example_client_1;

import lombok.Data;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class ControllerTest {
    @GetMapping("getUser")
    public User getUser() {
        User user = new User();
        user.setName("__________whwhwhwh ");
        return user;
    }

    @Data
    public static class User {
        public String name;
    }
}
